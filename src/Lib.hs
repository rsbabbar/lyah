module Lib
    ( helloWorld
    , quicksort
    , distinct
    , search
    )
where

import           Data.List
import qualified Data.Set                      as Set

helloWorld :: IO ()
helloWorld = putStrLn "Hello, World!"

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x : xs) =
    let smaller = quicksort $ filter (<= x) xs
        bigger  = quicksort $ filter (> x) xs
    in  smaller ++ [x] ++ bigger

distinct :: (Ord a) => [a] -> [a]
distinct xs = Set.toList $ Set.fromList xs

search :: (Eq a) => [a] -> [a] -> Bool
search needle haystack =
    let nlen = length needle
    in  foldl (\acc x -> take nlen x == needle || acc) False (tails haystack)
